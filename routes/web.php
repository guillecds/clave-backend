<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// USUARIOS
Route::post('/api/changeRecoverPass','UserController@changeRecoverPass');
Route::post('/api/recoverPass','UserController@recoverPass');
Route::post('/api/changePass','UserController@changePass');
Route::post('/api/updateUser','UserController@updateUser');
Route::post('/api/getUser','UserController@getUser');
Route::post('/api/registro', 'UserController@registro');
Route::post('/api/login', 'UserController@login');
Route::get('/api/getUserList','UserController@getUserList');
Route::post('/api/getRelList','UserController@getRelList');
Route::delete('/api/eliminarUsuario/{id}','UserController@eliminarUsuario');
Route::put('/api/restablecerPassword/{id}','UserController@restablecerPassword');
Route::delete('/api/eliminarRelacion/{id}','UserController@eliminarRelacion');
Route::post('/api/agregarRelacion/{idUser}/{idProf}','UserController@agregarRelacion');


//Consultorios
Route::get('/api/getConsultoriosProfesional/{id}','Consultorios@getConsultoriosProfesional');
Route::post('/api/storeConsultorio','Consultorios@storeConsultorio');
Route::delete('/api/destroyConsultorio/{id}','Consultorios@destroyConsultorio');
Route::put('/api/updateConsultorio','Consultorios@updateConsultorio');

//Horarios
Route::get('/api/getHorariosProfesional/{id}','Horarios@getHorariosProfesional');
Route::delete('/api/destroyHorario/{id}','Horarios@destroyHorario');
Route::post('/api/storeHorario','Horarios@storeHorario');
Route::put('/api/updateHorario','Horarios@updateHorario'); 

//Profesionales
Route::get('/api/getProfesionales','Profesionales@getProfesionales');
Route::get('/api/getProfesionalesFull','Profesionales@getProfesionalesFull');
Route::post('/api/getHoraProf','Profesionales@getHoraProf');
Route::delete('/api/destroyProfesional/{id}','Profesionales@destroyProfesional');
Route::post('/api/storeProfesional','Profesionales@storeProfesional');
Route::put('/api/updateProfesional','Profesionales@updateProfesional');
Route::post('/api/getProfesionalId','Profesionales@getProfesionalId');
// Route::get('/api/getAtencionProfesional/{id}','Profesionales@getAtencionProfesional');


//Citas
Route::get('/api/getCitaPorProfesional/{id}','Citas@getCitaPorProfesional');
Route::get('/api/getCitaPorProfesionalFixed/{id}','Citas@getCitaPorProfesionalFixed');
Route::post('/api/getCitaPorProfesionalXSemanaAnteriorFixed','Citas@getCitaPorProfesionalXSemanaAnteriorFixed');
Route::post('/api/getCitaPorProfesionalXSemanaSiguienteFixed','Citas@getCitaPorProfesionalXSemanaSiguienteFixed');
Route::post('/api/getCitaPorProfesionalXSemanaAnterior','Citas@getCitaPorProfesionalXSemanaAnterior');
Route::post('/api/getCitaPorProfesionalXSemanaSiguiente','Citas@getCitaPorProfesionalXSemanaSiguiente');
Route::get('/api/getCitasPendientes/{id}','Citas@getCitasPendientes');
Route::put('/api/updateStateCita/{id}','Citas@updateStateCita');
Route::put('/api/updateStateAtencion','Citas@updateStateAtencion');
Route::post('/api/getCita', 'Citas@getCita');
Route::post('/api/addCita','Citas@addCita');
Route::get('/api/getCitaEstadistica/{id}', 'Citas@getCitaEstadistica');

//Historias
Route::get('/api/getHistorias/{id}','Historias@getHistorias');
Route::post('/api/storeHistoria','Historias@storeHistoria');

//Pacientes
Route::get('/api/getPacientes','Pacientes@getPacientes');
Route::get('/api/getAllPacientes','Pacientes@getAllPacientes');
Route::delete('/api/destroyPaciente/{id}','Pacientes@destroyPaciente');
Route::post('/api/storePaciente','Pacientes@storePaciente');
Route::post('/api/pacientexdoc','Pacientes@pacientexdoc');
Route::put('/api/updatePaciente','Pacientes@updatePaciente');
Route::post('/api/upload','Pacientes@guardarEstudio');
Route::post('/api/estudioxpaciente','Pacientes@getEstudiosxPac');
Route::post('/api/getHcInfo','Pacientes@getHcInfo');


//Atencion
Route::get('/api/getAtencionPorProfesional/{id}','Listas@getAtencionPorProfesional');

//Especialidades
Route::get('/api/getEspecialidades','Especialidades@getEspecialidades');
Route::delete('/api/destroyEspecialidad/{id}','Especialidades@destroyEspecialidad');
Route::post('/api/storeEspecialidad','Especialidades@storeEspecialidad');
Route::post('/api/updateEspecialidad/','Especialidades@updateEspecialidad');


//Obras Sociales
Route::get('/api/getObrasSociales','ObrasSociales@getObrasSociales');
Route::delete('/api/destroyObraSocial/{id}','ObrasSociales@destroyObraSocial');
Route::post('/api/storeObraSocial','ObrasSociales@storeObraSocial');
Route::post('/api/updateObraSocial','ObrasSociales@updateObraSocial');

//Antecedentes
Route::get('/api/getAntecedentes/{id}','Antecedentes@getAntecedentes');
Route::post('/api/updateAntecedentes','Antecedentes@updateAntecedentes');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
