@component('mail::message')
# Asistente para la recuperación de contraseña

Hola<br>
Estás recibiendo este correo porque hiciste una solicitud de recuperación de contraseña para tu cuenta.<br>
<br>Si no realizaste esta solicitud, no se requiere realizar ninguna otra acción.<br>

@component('mail::button', ['url' => 'http://localhost:4200/#/changepw?token='.$token.'&email='.$email])
Recuperar contraseña
@endcomponent

Gracias,<br>
Equipo técnico de Clave Médica<br>
@endcomponent
