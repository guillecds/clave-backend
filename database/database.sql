CREATE DATABASE IF NOT EXISTS apilaravel;
USE apilaravel;

CREATE TABLE users(
    id int(255) auto_increment not null,
    email varchar (255),
    rol varchar(20),
    cliente varchar (255),
    password varchar (255),
    created_at datetime DEFAULT NULL,
    updated_at datetime DEFAULT NULL,
    remember_token varchar (255),
    CONSTRAINT pk_users PRIMARY KEY (id)
)ENGINE = InnoDb;


-- -----------------------------------------------------
-- Table `mydb`.`vendedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `vendedor` (
  `dni` INT NOT NULL COMMENT '',
  `nombre` VARCHAR(45) NULL COMMENT '',
  `apellido` VARCHAR(45) NULL COMMENT '',
  `mail` VARCHAR(100) NULL COMMENT '',
  `tel1` VARCHAR(45) NULL COMMENT '',
  `tel2` VARCHAR(45) NULL COMMENT '',
  `domicilio` VARCHAR(255) NULL COMMENT '',
  PRIMARY KEY (`dni`)  COMMENT '',
  INDEX `cod_telefono_idx` (`cod_telefono` ASC)  COMMENT '',
  CONSTRAINT `cod_telefono`
    FOREIGN KEY (`cod_telefono`)
    REFERENCES `telefono` (`codTelefono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `cliente` (
  `codCliente` INT NOT NULL COMMENT '',
  `nombre` VARCHAR(100) NULL COMMENT '',
  `cuit` VARCHAR(45) NULL COMMENT '',
  `domicilio` VARCHAR(255) NULL COMMENT '',
  `tel1` VARCHAR(45) NULL COMMENT '',
  `tel2` VARCHAR(45) NULL COMMENT '',
  `dni_vendedor` INT NULL COMMENT '',
  `mail` VARCHAR(100) NULL COMMENT '',
  PRIMARY KEY (`codCliente`)  COMMENT '',
  INDEX `dni_vendedor_idx` (`dni_vendedor` ASC)  COMMENT '',
  CONSTRAINT `dni_vendedor`
    FOREIGN KEY (`dni_vendedor`)
    REFERENCES `vendedor` (`dni`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `proveedor` (
  `codProveedor` INT NOT NULL COMMENT '',
  `descripcion` VARCHAR(255) NULL COMMENT '',
  `domicilio` VARCHAR(255) NULL COMMENT '',
  `provincia` VARCHAR(45) NULL COMMENT '',
  `tel1` VARCHAR(45) NULL COMMENT '',
  `tel2` VARCHAR(45) NULL COMMENT '',
  `mail` VARCHAR(100) NULL COMMENT '',
  PRIMARY KEY (`codProveedor`)  COMMENT '',
  INDEX `cod_telefono_idx` (`cod_telefono` ASC)  COMMENT '',
  CONSTRAINT `cod_telefono_proveedor`
    FOREIGN KEY (`cod_telefono`)
    REFERENCES `telefono` (`codTelefono`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`articulo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `articulo` (
  `codArticulo` INT NOT NULL COMMENT '',
  `cod_proveedor` INT NULL COMMENT '',
  `descripcion` VARCHAR(255) NULL COMMENT '',
  `importe` FLOAT NULL COMMENT '',
  `costo` FLOAT NULL COMMENT '',
  `flete` FLOAT NULL COMMENT '',
  `incremento` FLOAT NULL COMMENT '',
  `stock` INT NULL COMMENT '',
  PRIMARY KEY (`codArticulo`)  COMMENT '',
  INDEX `cod_proveedor_idx` (`cod_proveedor` ASC)  COMMENT '',
  CONSTRAINT `cod_proveedor`
    FOREIGN KEY (`cod_proveedor`)
    REFERENCES `proveedor` (`codProveedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`order`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `order` (
  `idOrder` INT auto_increment not null COMMENT '',
  `fecha` DATETIME DEFAULT NULL COMMENT '',
  `id_cliente` INT NOT NULL COMMENT '',
    created_at datetime DEFAULT NULL,
    updated_at datetime DEFAULT NULL,
    `observacion` TEXT DEFAULT NULL COMMENT '',
  PRIMARY KEY (`idOrder`)  COMMENT '',
  INDEX `id_cliente_idx` (`id_cliente` ASC)  COMMENT '',
  CONSTRAINT `id_cliente`
    FOREIGN KEY (`id_cliente`)
    REFERENCES `cliente` (`codCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `mydb`.`orderLine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `orderline` (
  `IdOrderLine` INT auto_increment not null COMMENT '',
  `cantidad` INT NOT NULL COMMENT '',
  `id_order` INT NOT NULL COMMENT '',
  `id_articulo` INT NOT NULL COMMENT '',
   created_at datetime DEFAULT NULL,
   updated_at datetime DEFAULT NULL,
  `total` FLOAT DEFAULT NULL COMMENT '',
  `unitPrice` FLOAT DEFAULT NULL COMMENT '',
  PRIMARY KEY (`IdOrderLine`)  COMMENT '',
  INDEX `id_order_idx` (`id_order` ASC)  COMMENT '',
  INDEX `id_articulo_idx` (`id_articulo` ASC)  COMMENT '',
  CONSTRAINT `id_order`
    FOREIGN KEY (`id_order`)
    REFERENCES `order` (`idOrder`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    CONSTRAINT `id_articulo`
    FOREIGN KEY (`id_articulo`)
    REFERENCES `articulo` (`codArticulo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;




SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
