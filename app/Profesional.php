<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesional extends Model
{   
    protected $fillable = [
        'apynom',
        'sexo',
        'tipodoc',
        'doc',
        'mail',
        'matricula',            
        'especialidad',
        'tel',
        'cel',
        'nac',
        'direcc',
        'loc',
        'cp',
        'prov',
        'citaminutos',
        'citaconhora',
        'notas',
        'config'
    ];

    protected $table = 'profesionales';
    protected $primaryKey = "id";
    
}
