<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Antecedente extends Model
{   
    protected $table = 'antecedentes';
    protected $primaryKey = "id";
    protected $fillable = ['idpac','creacion'];
    
}
