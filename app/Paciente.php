<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{   
    protected $fillable = [
        'apynom',
        'sexo',
        'tipodoc',
        'doc',
        'obrasocial',
        'mail',
        'tel',
        'cel',
        'nac',
        'direcc',
        'loc',
        'ficha',            
        'cp',
        'prov',
        'estciv'
    ];
    protected $table = 'pacientes';
    protected $primaryKey = "id";
}
