<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Usu_prof;
use App\Ingreso;
use Illuminate\Support\Facades\DB;
use App\Helpers\JwtAuth;
use Illuminate\Auth\Passwords\CanResetPassword;
use App\Notifications\MyResetPassword;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;


class UserController extends Controller
{
    public function registro (Request $request) {
        $json= $request->input('json',null);
        $params = json_decode($json);

        if(!is_null($json)){
           
            $password = (isset($params->password) ? $params->password : null);
            $username = (isset($params->user) ? $params->user : null);
            $nivel = (isset($params->nivel) ? $params->nivel : null);
            $email = (isset($params->email) ? $params->email : null);
            $idprof = (isset($params->idprof) ? $params->idprof : 0);
        }

        if(!is_null($username) && !is_null($password)){

            $isset_user = User::where('username','=',$username)->first();

            if(@count($isset_user) == 0){
               
                $user = new User();
                $user->username = $username;
                $user->nivel = $nivel;
                $user->email = $email;
                $user->idprof = $idprof;
                $pwd = hash('sha256',$password);
                $user->password = $pwd;
                $user->save();
                
                if($idprof){
                    $relacion = new Usu_prof();
                    $relacion->id_usuario = $user->id;
                    $relacion->id_profesional = $idprof;
                    $relacion->save();
                }

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'msj' => 'Usuario creado exitosamente',
                    'sub' => $user->id
                );

            }else{
                //NO GUARDAR USUARIO PORQUE YA EXISTE

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'msj' => 'El usuario ya existe'
                );
            }

        }else{
            $data = array(
                'status' => 'error',
                'code' => 400,
                'msj' => 'Usuario no creado'
            );


        }

        return response()->json($data,200); 
    }
    public function eliminarUsuario (Request $request, $id=null) {

        if ($id) {

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            $data = array();

            if($checkToken){

                $user = User::find($id);       
                $user->delete();

                if ($user) {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Usuario borrado con éxito',
                        'user' => $user
                    );
                } else {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => ' ocurrio un problema al eliminar el usuario'
                    );
                }

            } else {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                );
            }

        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Faltan datos'
            );
        }

        return response()->json($data, 200);
    }

    public function restablecerPassword (Request $request, $id=null) {

        if ($id) {

            $hash = $request->header('Authorization');
            $data = array();
            $user = User::find($id);       
            $pwd = hash('sha256','1234');
            $user->password = $pwd;
            $user->save();
            
            if ($user) {
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'message' => 'Usuario actualizado con éxito',
                    'user' => $user );
                } else {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => ' ocurrio un problema al reestablecer la clave'
                    );
                }

        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Faltan datos'
            );
        }

        return response()->json($data, 200);
    }

    public function updateUser(Request $request) {

        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        $user = $jwtAuth->checkToken($hash, true);


        if ($checkToken) {

            $json = file_get_contents('php://input');
            $obj = json_decode($json);
            $params = json_decode($obj->params);
            $params_array = json_decode($obj->params, true);
            $idUser = json_decode($obj->cod);

            $validate = \Validator::make($params_array, [                
                'email' => 'required',
                'rol' => 'required'
            ]);

            if ($validate->fails()) {
                return response()->json($validate->errors(), 400);
            }

            if (!is_null($json) && isset($idUser) && isset($params->email) && isset($params->rol)) {
                
                $user->email = $email;
                $user->cliente = $cliente;
                $user->establishment_id = $establishment_id;
                $user->rol = $rol;
                $user->username = $username;

                $user = array(
                    'email' => $params->email,
                    'rol' => $params->rol,
                    'establishment_id' => $params->establishment_id ? $params->establishment_id : 1,
                    'cliente' => isset($params->cliente) ? $params->cliente : '',
                    'nya' => isset($params->nya) ? $params->nya: ''
                );

                User::where('id', $idUser)
                    ->update($user);
                
                $data = array(
                    'user' => $user,
                    'message' => 'user editado correctamente',
                    'status' => 'success',
                    'code' => 200
                );

            } else {
                $data = array(
                    'message' => 'Faltan datos',
                    'status' => 'error',
                    'code' => 400
                );
            }

        } else {

            $data = array(
                'message' => 'El usuario no esta autorizado',
                'status' => 'error',
                'code' => 400
            );
        }

        return response()->json($data, 200);
    }

    public function updateCC(Request $request, $id = null) {

        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        
        if ($checkToken) {

            if($id != null){                
                
                $user = User::find($id);
                $user->cliente = "Quiero Aumento de CC";
                $user->save();
                
                $data = array(
                    'user' => $user,
                    'message' => 'user editado correctamente',
                    'status' => 'success',
                    'code' => 200
                );

            } else {
                $data = array(
                    'message' => 'Faltan datos',
                    'status' => 'error',
                    'code' => 400
                );
            }

        } else {

            $data = array(
                'message' => 'El usuario no esta autorizado',
                'status' => 'error',
                'code' => 400
            );
        }

        return response()->json($data, 200);
    }

    public function login(Request $request){
        $jwtAuth = new JwtAuth();

        //Recibir los datos por post

        $json = $request->input('json',null);
        $params = json_decode($json);


        if(!is_null($json)){
            $username= (isset($params->username) ? $params->username : null);
            $password = (isset($params->password) ? $params->password : null);
            $getToken = (isset($params->gettoken) ) ? $params->gettoken : null;


            //CIFRAR PASSWORD
            $pwd = hash('sha256',$password);

            if(!is_null($username) && !is_null($password) && (is_null($getToken) || $getToken == 'false')){
                $signup = $jwtAuth->signup($username,$pwd);

            }else if($getToken != null){
                // var_dump($getToken);die();
                $signup = $jwtAuth->signup($username,$pwd,$getToken);

            }else{
                $signup = array(
                    'status' => 'error',
                    'message' => 'Envia tus datos por post'
                );
            }
        }else {
            $signup = array(
                'status' => 'error',
                'message' => 'Envia tus datos por post'
            );
        }

        return response()->json($signup,200);
    }


    public function changePass (Request $request) {
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        $user = $jwtAuth->checkToken($hash, true);

        if ($checkToken) {

            //$email = $request->input('email');
            //$password = $request->input('password');
            //var_dump($email);

            $json = $request->input('json',null);


            if (!is_null($json)) {

                $params = json_decode($json);
                $email= (isset($params->email) ? $params->email : null);
                $password = (isset($params->password) ? $params->password : null);
                
                if(!is_null($email) && !is_null($password)){

                    //TRAIGO USUARIO A MODIFICAR
                    $userToChange = User::where('email',$email)->first();

                    //ENCRIPTADO PASSWORD
                    $pwd = hash('sha256',$password);
                    $userToChange->password = $pwd;

                    //GUARDAR USUARIO
                    $userToChange->save();

                } else{
                
                    $data = array(
                    'message' => 'Variables email o pass nulas',
                    'status' => 'error',
                    'code' => 400
                    );
                }

            } else {

                $data = array(
                'message' => 'Datos incompletos',
                'status' => 'error',
                'code' => 400
                );
            }

        } else {
            
            $data = array(
                'message' => 'El usuario no esta autorizado',
                'status' => 'error',
                'code' => 400
                );
        }

    $data = array(
        'message' => 'El cambio de contraseña fue realizado',
        'status' => 'success',
        'code' => 200
        );

    return response()->json($data, 200);
    }


    public function recoverPass (Request $request) {
        
        //$jwtAuth = new JwtAuth();
        $json = $request->input('json',null);

            if (!is_null($json)) {

                $params = json_decode($json);               
                $email= (isset($params->email) ? $params->email : null);
                if(!is_null($email)){

                    //TRAIGO USUARIO A MODIFICAR
                    $userToChange = User::where('email',$email)->first();
                    //GENERO TOKEN TEMPORAL PARA EL ENLACE Y EJECUTO EL ENVIO DE EMAIL AL USUARIO
                    $token = str_random(60);
                    if(is_null($userToChange->remember_token)){
                        $userToChange->remember_token = $token;
                        \Config::set('mail.username', 'usuarios@clavemedica.org');
                        \Config::set('mail.password', '*LmYw1C7dY');
                        Mail::to($email)->send(new ResetPasswordMail($token, $email));  

                        //GUARDAR USUARIO CON SU TOKEN TEMPORAL
                        $userToChange->save();
                    } else {
                        \Config::set('mail.username', 'usuarios@clavemedica.org');
                        \Config::set('mail.password', '*LmYw1C7dY');
                        Mail::to($email)->send(new ResetPasswordMail($userToChange->remember_token, $email));
                    }

                } else{
                
                    $data = array(
                    'message' => 'Email nulo',
                    'status' => 'error',
                    'code' => 400
                    );
                }

            } else {

                $data = array(
                'message' => 'Datos no recibidos',
                'status' => 'error',
                'code' => 400
                );
            }


        $data = array(
            'message' => 'Email de recuperación enviado correctamente',
            'status' => 'success',
            'code' => 200
        );

        return response()->json($data, 200);
    }

    


    public function changeRecoverPass (Request $request) {

            $json = $request->input('json',null);


            if (!is_null($json)) {

                $params = json_decode($json);
                $email= (isset($params->email) ? $params->email : null);
                $password = (isset($params->password) ? $params->password : null);
                $resetToken = (isset($params->resetToken) ? $params->resetToken : null);
                
                if(!is_null($email) && !is_null($password)){

                    //TRAIGO USUARIO A MODIFICAR
                    $userToChange = User::where('email',$email)->first();
                    
                    if($userToChange->remember_token==$resetToken)
                    {
                        //ENCRIPTADO PASSWORD
                        $pwd = hash('sha256',$password);
                        $userToChange->password = $pwd;
                        $userToChange->remember_token= NULL;

                        //GUARDAR USUARIO
                        $userToChange->save();
                    } else{
                        $data = array(
                            'message' => 'Autorización rechazada, token inválido',
                            'status' => 'error',
                            'code' => 400
                            );
                    }
                } else{
                
                    $data = array(
                    'message' => 'Variables email o pass nulas',
                    'status' => 'error',
                    'code' => 400
                    );
                }

            } else {

                $data = array(
                'message' => 'Datos incompletos',
                'status' => 'error',
                'code' => 400
                );
            }

    $data = array(
        'message' => 'El cambio de contraseña fue realizado',
        'status' => 'success',
        'code' => 200
        );

    return response()->json($data, 200);
    }




    public function getUser (Request $request) {
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        $user = $jwtAuth->checkToken($hash, true);

        if ($checkToken) {

            $json = $request->input('json',null);


            if (!is_null($json)) {

                $params = json_decode($json);
                $idUsuario= (isset($params->id) ? $params->id : null);
                
                if(!is_null($idUsuario)){

                    //TRAIGO USUARIO
                    $userToSend = User::find($idUsuario);

                    $data = array(
                        'user' => $userToSend,
                        'message' => 'Datos obtenidos',
                        'status' => 'success',
                        'code' => 200
                        );

                } else{
                
                    $data = array(
                    'message' => 'No se recibio id',
                    'status' => 'error',
                    'code' => 400
                    );
                }

            } else {

                $data = array(
                'message' => 'Datos incompletos',
                'status' => 'error',
                'code' => 400
                );
            }

        } else {
            
            $data = array(
                'message' => 'El usuario no esta autorizado',
                'status' => 'error',
                'code' => 400
                );
        }

   

    return response()->json($data, 200);
    }

    public function getUserList (Request $request) {
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {

            $userList = User::selectRaw('username, nivel, id, email')
                ->orderByRaw('username ASC')
                ->get();

                foreach($userList as $user){
                    if($user->nivel == "AD")
                    $user->nivel = "Administrador";
                    if($user->nivel == "AS")
                    $user->nivel = "Asistente";
                    if($user->nivel == "GS")
                    $user->nivel = "Supervisor de Grupo";
                    
                }
            $data = array(
                'data' => $userList,
                'message' => 'Usuarios obtenidos',
                'status' => 'success',
                'code' => 200
            );

        } else {

            $data = array(
                'message' => 'El usuario no esta autorizado',
                'status' => 'error',
                'code' => 400
            );

        }

        return response()->json($data, 200);

    }

    public function eliminarRelacion (Request $request, $id=null) {

        if ($id) {

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            $data = array();

            if($checkToken){

                $relacion = Usu_prof::find($id);    
                $relacion->delete();

                if ($relacion) {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Relacion borrada con éxito',
                        'relacion' => $relacion
                    );
                } else {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => ' ocurrio un problema al eliminar la relacion'
                    );
                }

            } else {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                );
            }

        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Faltan datos'
            );
        }

        return response()->json($data, 200);
    }

    public function agregarRelacion (Request $request, $idUser= null, $idProf = null) {

            $json= $request->input('json',null);
            $params = json_decode($json);

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            $data = array();

            // if(!is_null($json)){
            //     $idUser = (isset($params->idUser) ? $params->idUser : null);
            //     $idProfesional = (isset($params->idProfesional) ? $params->idProfesional : null);
            // }

            $relacion = new Usu_prof();
            $relacion->id_usuario = $idUser;
            $relacion->id_profesional = $idProf;
            $relacion->save();

         
                if ($relacion) {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Relacion agregada con éxito',
                        'relacion' => $relacion
                    );
                } else {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => ' ocurrio un problema al agregar la relacion'
                    );
                }

            
        

        return response()->json($data, 200);
    }



    public function getRelList (Request $request) {
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {

            $json = $request->input('json',null);

            if (!is_null($json)) {

                $params = json_decode($json);
                $idUsuario= (isset($params->idUser) ? $params->idUser : null);
                
                if(!is_null($idUsuario)){
                    $relList = User::selectRaw('users.username as usuario, profesionales.apynom as nombreprof, usu_prof.id as id, usu_prof.id_profesional as idprof')
                    ->orderByRaw('id ASC')
                    ->join('usu_prof', 'users.id', '=', 'usu_prof.id_usuario')
                    ->join('profesionales', 'usu_prof.id_profesional', '=', 'profesionales.id')
                    ->where('usu_prof.id_usuario', $idUsuario)
                    ->get();

                    $data = array(
                        'data' => $relList,
                        'message' => 'Relaciones obtenidas',
                        'status' => 'success',
                        'code' => 200
                    );
                } else {
                    $data = array(
                        'message' => 'El usuario está vacío',
                        'status' => 'error',
                        'code' => 400
                    ); 
                }
            } else {
                $data = array(
                    'message' => 'Datos no obtenidos',
                    'status' => 'error',
                    'code' => 400
                ); 
            }
        } else {
            $data = array(
                'message' => 'El usuario no esta autorizado',
                'status' => 'error',
                'code' => 400
            );
        }

        return response()->json($data, 200);

    }


}
