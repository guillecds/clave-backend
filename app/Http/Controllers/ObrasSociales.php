<?php
namespace App\Http\Controllers;
use App\ObraSocial;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;

class ObrasSociales extends Controller
{

    public function getObrasSociales(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken)
        {
            $obrassociales = ObraSocial::select('obrassociales.*')->orderBy('nombre', 'ASC')
                ->get();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $obrassociales
            );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
        return response()->json($data, 200);
    }

    public function destroyObraSocial(Request $request, $id = null)
    {
        // IMPROVE
        if ($id)
        {
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            if ($checkToken)
            {

                $obrassocial = ObraSocial::find($id);
                $obrassocial->delete();

                if ($obrassocial)
                {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Obra social borrada con éxito',
                        'data' => $obrassocial
                    );
                }
                else
                {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ocurrio un problema al eliminar la obra social'
                    );
                }

            }
            else
            {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                );
            }

        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado.'
            );
        }

        return response()->json($data, 200);
    }

    public function storeObraSocial(Request $request)
    {
        $rules = [
            'nombre' => 'required|unique:obrassociales,nombre|string',
            'sigla' => 'required|string',
            'description'=> 'string'
            ];
        if (!$this->validate($request, $rules)) return response()->json($error, 422);

        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
        if ($checkToken)
        {
            $params = $request->all();
            $obrasocial = ObraSocial::create($params);
            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $obrasocial,
                'message' => 'La obra social fue cargada de forma exitosa.'
            );

        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }

        return response()->json($data, 200);

    }

    public function updateObraSocial(Request $request)
    {
        // IMPROVE
        $rules = [
            'nombre' => 'string',
            'sigla' => 'string',
            'description'=> 'string'
            ];
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken)
        {
            
            $obrasocial = ObraSocial::find($request->id);

            if ($request->has('nombre'))
            {
                $obrasocial->nombre = $request->nombre;
            }

            if ($request->has('sigla'))
            {
                $obrasocial->sigla = $request->sigla;
            }

            if ($request->has('descripcion'))
            {
                $obrasocial->descripcion = $request->descripcion;
            }

            if (!$obrasocial->save())
            {
                return $this->errorResponse('Fail updating obra social', 409);
            }

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $obrasocial,
                'message' => 'La obra social fue actualizada de forma exitosa.'
            );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }

        return response()->json($data, 200);

    }

}

