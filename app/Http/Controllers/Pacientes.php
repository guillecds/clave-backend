<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\Paciente;
use App\Estudios;
use App\Historia;
use App\Antecedente;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;

class Pacientes extends Controller{

    //TO-DO: método getPacientes por profesional!!!
    
    public function getAllPacientes(Request $request)
    {
   
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        
        if ($checkToken) {

        // $pacientes = Paciente::select('id', 'apynom', 'sexo', 'tipodoc', 'doc', 'obrasocial', 'mail', 'tel', 'cel', 'nac', 'direcc', 'loc', 'ficha', 'cp', 'prov', 'estciv')
        // $pacientes = Paciente::select('id', 'apynom', 'sexo', 'tipodoc', 'doc', 'obrasocial', 'mail', 'tel', 'cel', 'nac', 'direcc', 'loc', 'ficha', 'cp', 'prov', 'estciv', 'obrasocial.obrasocial as os')
        $pacientes = Paciente::selectRaw('pacientes.*, obrassociales.nombre as obrasocial')
        ->distinct('apynom')
        ->orderBy('apynom', 'ASC')
        ->join('obrassociales', 'pacientes.obrasocial', '=', 'obrassociales.id')
        ->get();

        $data = array(
            'status' => 'success',
            'code' => 200,
            'data' =>  $pacientes

        );

        }
        else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }

        return response()->json($data, 200);

    }
    public function getPacientes(Request $request)
    {
   
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
        if ($checkToken) {

        $pacientes=Paciente::selectRaw('pacientes.*, profesionales.apynom as profesional')
        ->join('atencion', 'pacientes.id', 'atencion.idpac')
        ->join('profesionales', 'atencion.idprof', 'profesionales.id')
        ->orderBy('apynom', 'ASC')
        ->get();

    
        $data = array(
            'status' => 'success',
            'code' => 200,
            'data' =>  $pacientes

        );

        }
        else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }

        return response()->json($data, 200);

    }


    public function updatePaciente(Request $request)
    {
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
         
            if ($checkToken) {

                $paciente = Paciente::find($request->id);
    
                if ($request->has('apynom'))
                {
                    $paciente->apynom = $request->apynom;
                }

                if ($request->has('sexo'))
                {
                    $paciente->sexo = $request->sexo;
                }
                
                if ($request->has('tipodoc'))
                {
                    $paciente->tipodoc = $request->tipodoc;
                }

                if ($request->has('doc'))
                {
                    $paciente->doc = $request->doc;
                }
                  
                if ($request->has('obrasocial'))
                {
                    $paciente->obrasocial = $request->obrasocial;
                }
                  
                if ($request->has('mail'))
                {
                    $paciente->mail = $request->mail;
                }
                  
                if ($request->has('tel'))
                {
                    $paciente->tel = $request->tel;
                }
                  
                if ($request->has('cel'))
                {
                    $paciente->cel = $request->cel;
                }
                  
                if ($request->has('nac'))
                {
                    $paciente->nac = $request->nac;
                }
                  
                if ($request->has('direcc'))
                {
                    $paciente->direcc = $request->direcc;
                }
                  
                if ($request->has('loc'))
                {
                    $paciente->loc = $request->loc;
                }
                  
                if ($request->has('ficha'))
                {
                    $paciente->ficha = $request->ficha;
                }
                  
                if ($request->has('cp'))
                {
                    $paciente->cp = $request->cp;
                }
                  
                if ($request->has('prov'))
                {
                    $paciente->prov = $request->prov;
                }
                  
                if ($request->has('estciv'))
                {
                    $paciente->estciv = $request->estciv;
                }

                if (!$paciente->save())
                {
                    return $this->errorResponse('Fail updating paciente', 409);
                }
                    
                $data = array(
                    'paciente' => $paciente,
                    'status' => 'success',
                    'code' => 200
                );
       
            } else {
               
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }

            return response()->json($data, 200);
    }


    public function storePaciente(Request $request)
    {
        $rules = [
            'apynom' => 'required|string',
            'sexo' => 'required|string',
            'tipodoc' => 'required|string',
            'doc' => 'required|string',
            'obrasocial' => 'required|integer', 
            'mail' => 'string|nullable',
            'tel' => 'string|nullable',
            'cel' => 'string|nullable',
            'nac' => 'required|date|nullable',
            'direcc' => 'string|nullable',
            'loc' => 'string|nullable',
            'ficha' => 'string|nullable',
            'cp' => 'string|nullable',
            'prov' => 'required|string',
            'estciv' => 'string|nullable'
        ];

      
        if (!$this->validate($request, $rules)) return response()->json($error, 422);
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
            
        if ($checkToken) {
            
            $params = $request->all();
            $paciente = Paciente::create($params);
            
            $now =  Carbon::now()->timestamp;
            Antecedente::create([
                'idpac' => $paciente->id,
                'creacion' =>  $now
            ]);
            
            $data = array(
                'data' => $paciente,
                'status' => 'success',
                'code' => 200
            );
            }
             else 
            {
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }
        return response()->json($data, 200);
    }

    public function destroyPaciente(Request $request, $id=null)
    {
        if ($id) {

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            $data = array();

            if($checkToken){

                $paciente = Paciente::find($id);       
                $paciente->delete();

                if ($paciente) {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Paciente borrado con éxito',
                        'paciente' => $paciente
                    );
                } else {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => ' ocurrio un problema al eliminar el paciente'
                    );
                }

            } else {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                );
            }

        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Faltan datos'
            );
        }

        return response()->json($data, 200);
    }

    public function pacientexdoc(Request $request) {  


        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);

        if ($checkToken) {

            $paciente = Paciente::selectRaw('apynom, id, sexo, doc, tel, cel, nac, direcc, loc, prov, obrasocial')
                ->where('doc', $params)
                ->first();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'paciente' => $paciente
            );

        } else {

            $data= [
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no autorizado'
            ];

        }


        return response()->json($data, 200);

    }

    public function getEstudiosxPac(Request $request) {  


        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);

        if ($checkToken) {
            $estudios = Estudios::selectRaw('idpac, idprof, file')
                ->where('idpac', $params)
                ->get();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'estudios' => $estudios
            );

        } else {

            $data= [
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no autorizado'
            ];

        }


        return response()->json($data, 200);

    }

    public function getHcInfo(Request $request)
    {
   
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);
        
        if ($checkToken) {
            
            $historias = Historia::selectRaw('idpac, fecha, motivo, diagnostico, tratamiento, proximacita')
            ->where('idpac', $params)
            ->orderBy('fecha', 'DESC')
            ->get();

            $antecedentes = Antecedente::selectRaw('idpac, creacion, apers, afam, evol, modificado')
            ->where('idpac', $params)
            ->get();

            foreach($historias as $historia) { 
                $fecha= $historia->fecha;
                $historia->fecha = date("d-m-Y H:i",$fecha);
            }

            foreach($antecedentes as $antecedente) { 
                $fecha= $antecedente->creacion;
                $antecedente->creacion = date("d-m-Y H:i",$fecha);

                $fecha2= $antecedente->modificado;
                $antecedente->modificado = date("d-m-Y H:i",$fecha2);
            }

            $data = array(
                'status' => 'success',
                'code' => 200,
                'historias' =>  $historias,
                'antecedentes' =>  $antecedentes,
            );

        }
        else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }

        return response()->json($data, 200);

    }
    
    public function guardarEstudio(Request $request) {
        
        if (isset($_FILES['estudio'])) {
            
            $estudio_tipo = $_FILES['estudio']['type'];
            $estudio_nombre = $_FILES['estudio']['name'];
            $directorio_final = "storage/estudios/" . $estudio_nombre;
            $directorio_guardar = "storage/estudios/" . $estudio_nombre;
            
            $estudio = [
                'idPac' => $request->idPac,
                'idProf' => null,
                'file' => $directorio_final
            ];

            if ($estudio_tipo == "image/jpeg" || $estudio_tipo == "image/jpg" || $estudio_tipo == "image/png" || $estudio_tipo == "application/pdf" || $estudio_tipo == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {

                if (move_uploaded_file($_FILES['estudio']['tmp_name'], $directorio_final)) {
                    
                    Estudios::create($estudio);

                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'directorio' => $directorio_guardar,
                        'msj' => 'Estudio cargado'
                    );
                    return response()->json($data, 200);
                } else {

                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'msj' => 'Error al mover el estudio al servidor'
                    );
                    return response()->json($data, 500);
                }
            } else {

                $data = array(
                    'status' => 'error',
                    'code' => 500,
                    'msj' => 'Formato no soportado'
                );
                return response()->json($data, 500);
            }
        } else {

            $data = array(
                'status' => 'error',
                'code' => 400,
                'msj' => 'No se recibio ningun archivo'
            );
            return response()->json($data, 500);
        }
    }

}