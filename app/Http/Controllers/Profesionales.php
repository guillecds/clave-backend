<?php
namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Profesional;
use App\Horario;
use App\Consultorio;

class Profesionales extends Controller{

    public function getProfesionales(Request $request)
    {
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            
            $profesionales = Profesional::selectRaw('id, apynom')
            ->orderByRaw('apynom ASC')
            ->get();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $profesionales
            );
        }  else  {

            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
                }

        return response()->json($data, 200);
    }

    public function getProfesionalId(Request $request)
    {
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);

        if ($checkToken) {
            
            $profesional = Profesional::selectRaw('id, apynom')
            ->where('id', $params->id)
            ->first();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $profesional
            );
        }  else  {

            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
                }

        return response()->json($data, 200);
    }

    public function getProfesionalesFull(Request $request)
    {
        // <!-- apynom tel especialidad consultorio -->
        
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken) {
            
            $profesionales = Profesional::selectRaw('profesionales.*, especialidades.especialidad as especialidad, consultorios.domicilio as consultorio')
            ->orderByRaw('apynom ASC')
            ->join('prof_espec', 'profesionales.id', '=', 'prof_espec.idprof')
            ->join('especialidades', 'prof_espec.idespec', '=', 'especialidades.id')
            ->join('consultorios', 'consultorios.idprof', '=', 'profesionales.id')
            ->get();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $profesionales
            );
        }  else  {

            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
                }

        return response()->json($data, 200);
    }


    public function getHoraProf(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);


        $json = $request->input('json', null);
        $params = json_decode($json);

        if($params->id) {

            if ($checkToken) {
                
                $minutos = Profesional::selectRaw('citaminutos')
                ->where('id', $params->id)
                ->get();

                $cons = Consultorio::selectRaw('id')
                ->where('idprof', $params->id)
                ->first();

                $horarios = Horario::selectRaw('dia, horaini, minini, horalargai, horafin, minfin, horalargaf, idcons')
                ->where('idprof', $params->id)
                //->where('idcons', $cons->id)
                ->get();

                $consultorios = Consultorio::selectRaw('DISTINCT id, nombre')
                ->where('idprof', $params->id)
                ->get();

                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'minutos' => $minutos,
                    'consultorios' => $consultorios,
                    'horarios' => $horarios
                );

            } 
            else {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no identificado'
                );

            }
        }

        else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Datos insuficientes'
            );
        }

        return response()->json($data, 200);
    }

    public function destroyProfesional(Request $request, $id=null)
    {
        if ($id) {

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
            $data = array();

            if($checkToken){

                $profesional= Profesional::find($id);       
                $profesional->delete();

                if ($profesional) {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'profesional borrado con éxito',
                        'profesional' => $profesional
                    );
                } else {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ocurrio un problema al eliminar el profesional'
                    );
                }

            } else {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                );
            }

        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Faltan datos'
            );
        }

        return response()->json($data, 200);
    }

    
    // public function getAtencionProfesional(Request $request, $id=null)
    // {   
    //     error_log($id);
    //     if ($id) {

    //         $hash = $request->header('Authorization');
    //         $jwtAuth = new JwtAuth();
    //         $checkToken = $jwtAuth->checkToken($hash);
    //         $data = array();

    //         if($checkToken){

               
    //             $profesional= Profesional::where('id', $id)
    //             ->get(['citaminutos', 'notas']);       

    //             error_log(json_encode($profesional, JSON_PRETTY_PRINT));
    //             if ($profesional) {
    //                 $data = array(
    //                     'status' => 'success',
    //                     'code' => 200,
    //                     'message' => 'datos del profesional devuelto con éxito',
    //                     'profesional' => $profesional
    //                 );
    //             } else {
    //                 $data = array(
    //                     'status' => 'error',
    //                     'code' => 400,
    //                     'message' => 'Ocurrio un problema al obtener los datos'
    //                 );
    //             }

    //         } else {

    //             $data = array(
    //                 'status' => 'error',
    //                 'code' => 400,
    //                 'message' => 'Usuario no autorizado'
    //             );
    //         }

    //     } else {
    //         $data = array(
    //             'status' => 'error',
    //             'code' => 400,
    //             'message' => 'Faltan datos'
    //         );
    //     }

    //     return response()->json($data, 200);
    // }


    public function updateProfesional(Request $request)
    {     
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
         
            if ($checkToken) {

                $json = $request->input('json', null);
                $params = json_decode($json);
                //TO-DO borrar todos los error_log
                error_log(json_encode($params, JSON_PRETTY_PRINT));
                $profesional = Profesional::find($params->id);
                $profesional->apynom = $params->apynom;
                $profesional->sexo = $params->sexo;
                $profesional->tipodoc = $params->tipodoc;
                $profesional->doc = $params->doc;
                $profesional->mail = $params->mail;
                $profesional->matricula = $params->matricula;
                $profesional->especialidad = $params->especialidad;
                $profesional->tel = $params->tel;
                $profesional->cel = $params->cel;
                $profesional->nac = $params->nac;
                $profesional->direcc = $params->direcc;
                $profesional->loc = $params->loc;
                $profesional->cp = $params->cp;
                // $profesional->citaminutos= $params->citaminutos;
                $profesional->notas= $params->notas;
                $profesional->prov = $params->prov;
                $profesional->save();
                  
                $data = array(
                    'profesional' => $profesional,
                    'status' => 'success',
                    'code' => 200
                );
       
            } else {
               
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }

            return response()->json($data, 200);
    }


    public function storeProfesional(Request $request)
    {      
        $rules = [
            'apynom' => 'required|string',
            'sexo' => 'string|nullable',
            'tipodoc' => 'required|string',
            'doc' => 'required|string',
            'mail' => 'required|string|nullable',
            'matricula' => 'required|integer', 
            'especialidad' => 'required|integer',
            'tel' => 'required|string',
            'cel' => 'string|nullable',
            'nac' => 'date|nullable',
            'direcc' => 'string|nullable',
            'loc' => 'string|nullable',
            'cp' => 'string|nullable',
            'prov' => 'required|string',
            // 'citaminutos' => 'string|nullable',
            // 'citaconhora' => 'string|nullable',
            'notas' => 'string|nullable',
            'config' => 'string|nullable'

        ];
        if (!$this->validate($request, $rules)) return response()->json($error, 422);

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);

            if ($checkToken) {

                $params = $request->all();
                // IMPROVE THIS

                $profesional = Profesional::create($params);
    
                $data = array(
                    'data' => $profesional,
                    'status' => 'success',
                    'code' => 200
                );
       
            } else {
               
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }

            return response()->json($data, 200);
    }



}


