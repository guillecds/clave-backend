<?php
namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Cita;
use App\Consultorios;
use App\Pacientes;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;
use App\Atencion;
use App\Paciente;
use App\Antecedente;

class Citas extends Controller{

    public function getCita(Request $request){

        $json = $request->input('json', null);
        $params = json_decode($json);

        if($params->id != null) {

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);

            if ($checkToken) {
         
                $citas=Cita::selectRaw('pacientes.apynom as paciente, pacientes.*, citas.*, obrassociales.nombre as nameobrasocial, consultorios.domicilio as consultorio, profesionales.apynom as prof')
                ->join('pacientes', 'pacientes.id', 'citas.idpac')
                ->join('profesionales', 'profesionales.id', 'citas.idprof')
                ->join('obrassociales', 'pacientes.obrasocial', 'obrassociales.id')
                ->join('consultorios', 'consultorios.id', 'citas.idcons')
                ->where('citas.id', $params->id)
                ->first();

                $fecha= $citas->fechalarga;
                $citas->fechalarga = date("d-m-Y H:i",$fecha);
                
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'cita' =>  $citas);

            }
            else {
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no identificado'
                );
            }
        } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }       
        return response()->json($data, 200);
    }

    public function getCitaPorProfesionalXSemanaAnterior(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);
        ini_set('max_execution_time', 300);
        if($params->id)
        {
  
        date_default_timezone_set("America/Argentina/San_Juan");
        if ($checkToken) {
            
            $now =  Carbon::now()->timestamp;
            $year = Carbon::now()->year;
            
            $fecha = $params->dia.'-'.$year.' 00:00:00';
            $dtime = DateTime::createFromFormat("d-m-Y H:i:s", $fecha);
            $end = $dtime->getTimestamp();
            $end -=  86400;
            $start = $end - (86400*6);
      
            $citas=Cita::selectRaw('dia, fechalarga as fecha, citas.notas as notas, pacientes.apynom as paciente, pacientes.tel, consultorios.domicilio as consultorio, estado, citas.idprof as idprof, pacientes.id as idpac, citas.id as id')
            ->join('pacientes','idPac','=','pacientes.id')
            ->join('consultorios','idCons','=','consultorios.id')
            ->where('citas.idprof', $params->id)
            ->whereBetween('citas.fechalarga',[$start,$end])
            ->orderBy('citas.fechalarga')
            ->get();
 
            foreach($citas as $cita) { 

                $fecha= $cita->fecha;
                $cita->fecha= date("d-m-Y H:i",$fecha);

                // $anterior=Cita::selectRaw('fechalarga as consultaprevia')
                // ->where('citas.idprof', $cita->idprof)
                // ->where('citas.idpac', $cita->idpac)
                // ->where('citas.id', '<', $cita->id)
                // ->orderBy('citas.id', 'desc')
                // ->first();
                
                // if(isset($anterior)){
                //     $cita->anterior= date("d-m-Y", $anterior->consultaprevia);
                // }
                
            }

            $comienzo=date("d-m",$start);
            $finde=date("d-m",$end);


            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $citas,
                'comienzosemana' =>  $comienzo,
                'finsemana' =>  $finde
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }
        return response()->json($data, 200);
    
    }

    public function getCitaPorProfesionalXSemanaSiguiente(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);
        ini_set('max_execution_time', 300);
        if($params->id)
        {
  
        date_default_timezone_set("America/Argentina/San_Juan");
        if ($checkToken) {
        
            $now =  Carbon::now()->timestamp;
            $year = Carbon::now()->year;
            $fecha = $params->dia.'-'.$year.' 00:00:00';
            $dtime = DateTime::createFromFormat("d-m-Y H:i:s", $fecha);
            $start= $dtime->getTimestamp();
            $start +=  86400;
            $end = $start + (86400*6);

            $citas=Cita::selectRaw('dia, fechalarga as fecha, citas.notas as notas, pacientes.apynom as paciente, pacientes.tel, consultorios.domicilio as consultorio, estado, citas.idprof as idprof, pacientes.id as idpac, citas.id as id')
            ->join('pacientes','idPac','=','pacientes.id')
            ->join('consultorios','idCons','=','consultorios.id')
            ->where('citas.idprof', $params->id)
            ->whereBetween('citas.fechalarga',[$start,$end])
            ->orderBy('citas.fechalarga')
            ->get();
 
            foreach($citas as $cita) { 

                $fecha= $cita->fecha;
                $cita->fecha= date("d-m-Y H:i",$fecha);

                // $anterior=Cita::selectRaw('fechalarga as consultaprevia')
                // ->where('citas.idprof', $cita->idprof)
                // ->where('citas.idpac', $cita->idpac)
                // ->where('citas.id', '<', $cita->id)
                // ->orderBy('citas.id', 'desc')
                // ->first();
                
                // if(isset($anterior)){
                //     $cita->anterior= date("d-m-Y", $anterior->consultaprevia);
                // }
            }

            $comienzo=date("d-m",$start);
            $finde=date("d-m",$end);

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $citas,
                'comienzosemana' =>  $comienzo,
                'finsemana' =>  $finde
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }

        return response()->json($data, 200);
            
    }

    public function getCitaPorProfesional(Request $request, $id=null)
    {
        if($id)
        {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
        ini_set('max_execution_time', 300); 
        date_default_timezone_set("America/Argentina/San_Juan");
        if ($checkToken) {
           
            $now =  Carbon::now()->timestamp;
            $hoy = Carbon::now();
            $start = $hoy->startOfWeek()->timestamp;
            $end = $hoy->endOfWeek()->timestamp;
            $week = $now + (86400*7);

            $citas=Cita::selectRaw('dia, fechalarga as fecha, citas.notas as notas, pacientes.apynom as paciente, pacientes.tel, consultorios.domicilio as consultorio, estado, citas.idprof as idprof, pacientes.id as idpac, citas.id as id')
            ->join('pacientes','idPac','=','pacientes.id')
            ->join('consultorios','idCons','=','consultorios.id')
            ->where('citas.idprof', $id)
            ->whereBetween('citas.fechalarga',[$start,$end])
            ->orderBy('citas.fechalarga')
            ->get();
 
            foreach($citas as $cita) { 

                $fecha= $cita->fecha;
                $cita->fecha= date("d-m-Y H:i",$fecha);

                // $anterior=Cita::selectRaw('fechalarga as consultaprevia')
                // ->where('citas.idprof', $cita->idprof)
                // ->where('citas.idpac', $cita->idpac)
                // ->where('citas.id', '<', $cita->id)
                // ->orderBy('citas.id', 'desc')
                // ->first();
                
                // if(isset($anterior)){
                //     $cita->anterior= date("d-m-Y", $anterior->consultaprevia);
                // }
            }

            $comienzo=date("d-m",$start);
            $finde=date("d-m",$end);



            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $citas,
                'comienzosemana' =>  $comienzo,
                'finsemana' =>  $finde
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }

        return response()->json($data, 200);
    }

    public function getCitaPorProfesionalFixed(Request $request, $id=null)
    {
        if($id) {
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
       
            date_default_timezone_set("America/Argentina/San_Juan");
            if ($checkToken) {
           
                $now =  Carbon::now()->timestamp;
                $hoy = Carbon::now();
                $start = $hoy->startOfWeek()->timestamp;
                $end = $hoy->endOfWeek()->timestamp;
                $week = $now + (86400*7);

                $weekArray = array(
                    date("d-m",$start),
                    date("d-m",$hoy->startOfWeek()->addDays(1)->timestamp),
                    date("d-m",$hoy->startOfWeek()->addDays(2)->timestamp),
                    date("d-m",$hoy->startOfWeek()->addDays(3)->timestamp),
                    date("d-m",$hoy->startOfWeek()->addDays(4)->timestamp),
                );

                 $citas=Cita::selectRaw('dia, fechalarga as fecha, citas.notas as notas, pacientes.apynom as paciente, pacientes.tel, consultorios.domicilio as consultorio, estado, citas.idprof as idprof, pacientes.id as idpac, citas.id as id')
                ->join('pacientes','idPac','=','pacientes.id')
                ->join('consultorios','idCons','=','consultorios.id')
                ->where('citas.idprof', $id)
                ->whereBetween('citas.fechalarga',[$start,$end])
                ->orderBy('citas.fechalarga')
                ->get();
 
                foreach($citas as $cita) { 

                    $fecha= $cita->fecha;
                    $cita->fecha= date("d-m-Y H:i",$fecha);
                
                }

                $comienzo=date("d-m",$start);
                $finde=date("d-m",$end);


                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'data' =>  $citas,
                    'comienzosemana' =>  $comienzo,
                    'finsemana' =>  $finde,
                    'semana' => $weekArray
                );
            } else {
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no identificado'
                );
            }
        } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }
        return response()->json($data, 200);
    }

    public function getCitaPorProfesionalXSemanaAnteriorFixed(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);
        ini_set('max_execution_time', 300);
        if($params->id)
        {
  
        date_default_timezone_set("America/Argentina/San_Juan");
        if ($checkToken) {
            
            $now =  Carbon::now()->timestamp;
            $year = Carbon::now()->year;
            
            $fecha = $params->dia.'-'.$year.' 00:00:00';
            $dtime = DateTime::createFromFormat("d-m-Y H:i:s", $fecha);
            $end = $dtime->getTimestamp();
            $end -=  86400;
            $start = $end - (86400*6);

            $weekArray = array(
                date("d-m",$start),
                date("d-m",$start + (86400)),
                date("d-m",$start + (86400*2)),
                date("d-m",$start + (86400*3)),
                date("d-m",$start + (86400*4)),
            );
      
            $citas=Cita::selectRaw('dia, fechalarga as fecha, citas.notas as notas, pacientes.apynom as paciente, pacientes.tel, consultorios.domicilio as consultorio, estado, citas.idprof as idprof, pacientes.id as idpac, citas.id as id')
            ->join('pacientes','idPac','=','pacientes.id')
            ->join('consultorios','idCons','=','consultorios.id')
            ->where('citas.idprof', $params->id)
            ->whereBetween('citas.fechalarga',[$start,$end])
            ->orderBy('citas.fechalarga')
            ->get();
 
            foreach($citas as $cita) { 

                $fecha= $cita->fecha;
                $cita->fecha= date("d-m-Y H:i",$fecha);
                
            }

            $comienzo=date("d-m",$start);
            $finde=date("d-m",$end);


            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $citas,
                'comienzosemana' =>  $comienzo,
                'finsemana' =>  $finde,
                'semana' => $weekArray
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }
        return response()->json($data, 200);
    
    }

    public function getCitaPorProfesionalXSemanaSiguienteFixed(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);
        ini_set('max_execution_time', 300);
        if($params->id)
        {
  
        date_default_timezone_set("America/Argentina/San_Juan");
        if ($checkToken) {
        
            $now =  Carbon::now()->timestamp;
            $year = Carbon::now()->year;
            $fecha = $params->dia.'-'.$year.' 00:00:00';
            $dtime = DateTime::createFromFormat("d-m-Y H:i:s", $fecha);
            $start= $dtime->getTimestamp();
            $start +=  86400;
            $end = $start + (86400*6);

            $weekArray = array(
                date("d-m",$start),
                date("d-m",$start + (86400)),
                date("d-m",$start + (86400*2)),
                date("d-m",$start + (86400*3)),
                date("d-m",$start + (86400*4)),
            );

            $citas=Cita::selectRaw('dia, fechalarga as fecha, citas.notas as notas, pacientes.apynom as paciente, pacientes.tel, consultorios.domicilio as consultorio, estado, citas.idprof as idprof, pacientes.id as idpac, citas.id as id')
            ->join('pacientes','idPac','=','pacientes.id')
            ->join('consultorios','idCons','=','consultorios.id')
            ->where('citas.idprof', $params->id)
            ->whereBetween('citas.fechalarga',[$start,$end])
            ->orderBy('citas.fechalarga')
            ->get();
 
            foreach($citas as $cita) { 

                $fecha= $cita->fecha;
                $cita->fecha= date("d-m-Y H:i",$fecha);

            }

            $comienzo=date("d-m",$start);
            $finde=date("d-m",$end);

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $citas,
                'comienzosemana' =>  $comienzo,
                'finsemana' =>  $finde,
                'semana' => $weekArray
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }

        return response()->json($data, 200);
            
    }


    public function getCitasPendientes(Request $request, $id=null)
    {
        if($id)
        {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
  

        if ($checkToken) {
        
             $citas=Cita::selectRaw('pacientes.apynom as paciente, pacientes.doc as doc, citas.id as id')
            ->join('pacientes','idPac','=','pacientes.id')
            ->join('consultorios','idCons','=','consultorios.id')
            ->where('citas.idprof', $id)
            ->where ('citas.estado','PENDIENTE')
            ->take(10)
            ->get();
 
            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $citas
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }

        return response()->json($data, 200);
    }

    public function updateStateCita(Request $request, $idCita=null) 
    {  
        if($idCita)
        {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);

        if ($checkToken) {
            
            $cita= Cita::find($idCita);
            $cita->estado= $params;
            $cita->save();



            if($params == 'EN_SALA')
            {
                $atencionToAdd = new Atencion();
                $atencionToAdd->idcita = $cita->id;
                $atencionToAdd->idpac = $cita->idpac;
                $atencionToAdd->idprof = $cita->idprof;
                $atencionToAdd->fechalarga = $cita->fechalarga;
                $atencionToAdd->save();
            } else {
                $atencionToRemove = Atencion::where('idcita', '=', $idCita)
                ->first();

                if ($atencionToRemove)
                    $atencionToRemove->delete();
            }

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $cita
            );
        }  else {
                $data= [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                ];
            }

        }  else {
        $data = [
            'status' => 'error',
            'code' => 400,
            'message' => 'faltan datos'
        ];
                }
        return response()->json($data, 200);

    
    }

    public function updateStateAtencion(Request $request) 
    {  
        
 
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);
  
        if ($checkToken) {
            $idCita= $params->idCita;
            $cita= Cita::find($idCita);
            $cita->estado= 'ATENDIDA';
            $cita->save();

            $idAtencion= $params->idAtencion;
            $atencion = Atencion::find($idAtencion);
         
         
            $atencion->delete();
            
            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $cita
            );
        }  else {
                $data= [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                ];
            }

        /* }  else {
        $data = [
            'status' => 'error',
            'code' => 400,
            'message' => 'faltan datos'
        ];
                } */
        return response()->json($data, 200);


    }

    public function addCita(Request $request) {  


        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        $json = $request->input('json', null);
        $params = json_decode($json);

        if ($checkToken) {
            
            $cita = $params->cita;
            $paciente = $params->paciente;
            
            
            $pacienteToAdd = Paciente::where('doc', $paciente->doc)
            ->first();

            if(!$pacienteToAdd){
                $pacienteToAdd = new Paciente();
                $pacienteToAdd->apynom = $params->paciente->apynom;
                $pacienteToAdd->doc = $params->paciente->doc;
                $pacienteToAdd->obrasocial = $params->paciente->obrasocial;
                $pacienteToAdd->cel = $params->paciente->cel;
                $pacienteToAdd->sexo = $params->paciente->sexo;
                $pacienteToAdd->tel = $params->paciente->tel;
                $pacienteToAdd->direcc = $params->paciente->direcc;
                $pacienteToAdd->loc = $params->paciente->loc;
                $pacienteToAdd->prov = $params->paciente->prov;
                $pacienteToAdd->save();

                $nowdate =  Carbon::now()->timestamp;
                Antecedente::create([
                    'idpac' => $pacienteToAdd->id,
                    'creacion' =>  $nowdate
                ]);
            }

            $year = Carbon::now()->year;
            $fecha = $params->cita->comienzo.'-'.$year.' 00:00:00';
            $dtime = DateTime::createFromFormat("d-m-Y H:i:s", $fecha);
            $start= $dtime->getTimestamp();
            $start+=($params->cita->hora)/1000;
            if($params->cita->diaSemana == 'Mar')
                $start +=  (86400);
            if($params->cita->diaSemana == 'Mie')
                $start +=  (86400*2);
            if($params->cita->diaSemana == 'Jue')
                $start +=  (86400*3);
            if($params->cita->diaSemana == 'Vie')
                $start +=  (86400*4);
            if($params->cita->diaSemana == 'Sab')
                $start +=  (86400*5);

            $citaToAdd = new Cita();
            $citaToAdd->idprof = $params->cita->profesional->id ? $params->cita->profesional->id : null;
            $citaToAdd->idcons = $params->cita->idCons ? $params->cita->idCons : null;
            $citaToAdd->dia = $params->cita->diaSemana ? $params->cita->diaSemana : null;
            $citaToAdd->fechalarga = $start ? $start : null;
            $citaToAdd->idpac = $pacienteToAdd->id ? $pacienteToAdd->id : null;
            $citaToAdd->estado = 'PENDIENTE';
            $citaToAdd->notas = $params->cita->notas ? $params->cita->notas : null;
            $citaToAdd->tipodecita = $params->cita->tipocita ? $params->cita->tipocita : null;
            $citaToAdd->save();

            $atencionToAdd = new Atencion();
            $atencionToAdd->idcita = $citaToAdd->id;
            $atencionToAdd->idpac = $pacienteToAdd->id;
            $atencionToAdd->idprof = $params->cita->profesional->id;
            $atencionToAdd->fechalarga = $start;
            $atencionToAdd->save();


            $data = array(
                'status' => 'success',
                'code' => 200
            );

        } else {

            $data= [
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no autorizado'
            ];

        }
        return response()->json($data, 200);

    }


    public function getCitaEstadistica(Request $request, $id=null)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($id)
        {
  
        date_default_timezone_set("America/Argentina/San_Juan");
        if ($checkToken) {
            setlocale(LC_ALL,"es_ES"); 
            $now =  Carbon::now();
            $date =  Carbon::now()->timestamp;
            $firstOfMonth =  $now->firstOfMonth()->timestamp;
            // $firstOfYear = $now->subYear()->timestamp;
            $firstOfYear = $now->startOfYear()->timestamp;
            $year = Carbon::now()->year;
      
            $lastMonth=Cita::selectRaw('fechalarga, estado')
            ->where('idprof', $id)
            ->whereBetween('fechalarga',[$firstOfMonth,$date])
            ->orderBy('fechalarga', 'desc')
            ->get();

            // $lastYear=Cita::selectRaw('fechalarga, estado')
            // ->where('idprof', $id)
            // ->whereBetween('fechalarga',[$firstOfYear,$date])
            // ->orderBy('fechalarga', 'desc')
            // ->get();
           
            foreach (CarbonPeriod::create(date("Y-m-d", $firstOfYear), '1 month', date("Y-m-d", $firstOfMonth)) as $month) {
                $months[$month->format('m')] = $month->formatLocalized('%B');
            }

            $aux = $now->startOfYear()->timestamp;
            $aux2 = $now->startOfYear()->addMonths(1)->timestamp;
            $thisYearArray = array();

            for ($i=1; $i<=count($months); $i++){
                
                $atendidas = 0;
                $pendientes = 0;
                $ausentes = 0;
                $canceladas = 0;
                $anuladas = 0;

                $lastYear=Cita::selectRaw('fechalarga, estado')
                ->where('idprof', $id)
                ->whereBetween('fechalarga',[$aux,$aux2])
                ->orderBy('fechalarga', 'desc')
                ->get();

                foreach ($lastYear as $mes){
                    if ($mes->estado == 'ATENDIDA')
                        $atendidas ++;
                    if ($mes->estado == 'CANCELADA')
                        $canceladas ++;
                    if ($mes->estado == 'AUSENTE')
                        $ausentes ++;
                    if ($mes->estado == 'ANULADA')
                        $anuladas ++;
                    if ($mes->estado == 'PENDIENTE')
                        $pendientes ++;  
                }

                $mescontado = array(
                    'mes' => $months['0'.$i],
                    'atendidas' => $atendidas,
                    'pendientes' => $pendientes,
                    'ausentes' => $ausentes,
                    'canceladas' => $canceladas,
                    'anuladas' => $anuladas,
                );

                $aux = $aux+2629800;
                $aux2 = $aux2+2629800;
                array_push($thisYearArray, $mescontado);
            }

            $data = array(
                'status' => 'success',
                'code' => 200,
                'year' =>  $year,
                'lastMonth' =>  $lastMonth,
                'lastYear' =>  $thisYearArray
            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }
        return response()->json($data, 200);
    
    }
     
       
}