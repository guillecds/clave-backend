<?php
namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Profesional;
use App\Consultorio;

class Consultorios extends Controller{

    public function getConsultoriosProfesional(Request $request, $id = null)
        {
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
          
            if ($checkToken) {
                
                $consultorios = Consultorio::selectRaw('consultorios.*')
                ->where('consultorios.idprof', '=', $id)
                ->get();
            
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'data' => $consultorios
                );
            }  else  {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no identificado'
                );
                    }

            return response()->json($data, 200);

        }

        public function storeConsultorio(Request $request)
        {
            $rules = [
                'idprof' => 'required|integer', 
                'domicilio' => 'string',
                'localidad' => 'string',
                'provincia' => 'string',
                'telefono' => 'string',
                'nombre' => 'string',
                'notas' => 'string|nullable',
            ];
    
            if (!$this->validate($request, $rules)) return response()->json($error, 422);
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
                
            if ($checkToken) {
                
                $params = $request->all();
                $consultorio = Consultorio::create($params);
                $data = array(
                    'data' => $consultorio,
                    'status' => 'success',
                    'code' => 200
                );
                }
                 else 
                {
                    $data = array(
                        'message' => 'El usuario no esta autorizado',
                        'status' => 'error',
                        'code' => 400
                    );
                }
            return response()->json($data, 200);
        }

        public function destroyConsultorio(Request $request, $id=null)
        {
            if ($id) {
    
                $hash = $request->header('Authorization');
                $jwtAuth = new JwtAuth();
                $checkToken = $jwtAuth->checkToken($hash);
                $data = array();
    
                if($checkToken){
    
                    $consultorio = Consultorio::find($id);       
                    $consultorio->delete();
    
                    if ($consultorio) {
                        $data = array(
                            'status' => 'success',
                            'code' => 200,
                            'message' => 'Consultorio borrado con éxito',
                            'consultorio' => $consultorio
                        );
                    } else {
                        $data = array(
                            'status' => 'error',
                            'code' => 400,
                            'message' => ' ocurrio un problema al eliminar el consultorio'
                        );
                    }
    
                } else {
    
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Usuario no autorizado'
                    );
                }
    
            } else {
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Faltan datos'
                );
            }
    
            return response()->json($data, 200);
        }


        
    public function updateConsultorio(Request $request)
    {
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
         
            if ($checkToken) {

                $consultorio = Consultorio::find($request->id);
    
                if ($request->has('domicilio'))
                {
                    $consultorio->domicilio = $request->domicilio;
                }

                if ($request->has('localidad'))
                {
                    $consultorio->localidad = $request->localidad;
                }

                if ($request->has('provincia'))
                {
                    $consultorio->provincia = $request->provincia;
                }

                if ($request->has('telefono'))
                {
                    $consultorio->telefono = $request->telefono;
                }
                
                if ($request->has('nombre'))
                {
                    $consultorio->nombre = $request->nombre;
                }

                if ($request->has('notas'))
                {
                    $consultorio->notas = $request->notas;
                }

                if (!$consultorio->save())
                {
                    return $this->errorResponse('Fail updating consultorio', 409);
                }
                    
                $data = array(
                    'consultorio' => $consultorio,
                    'status' => 'success',
                    'code' => 200
                );
       
            } else {
               
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }

            return response()->json($data, 200);
    }
        

}

