<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\Antecedente;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;


class Antecedentes extends Controller{

    public function getAntecedentes(Request $request, $id=null) 
    {
        if($id)
        {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
            if ($checkToken) 
            {
                $antecedentes=Antecedente::select('antecedentes.*')
                ->where('antecedentes.idpac', '=', $id)
                ->first();
         
                $antecedentes = collect($antecedentes);

                

                $data = array
                (
                    'status' => 'success',
                    'code' => 200,
                    'antecedentes' =>  $antecedentes
                );
            } else 
                {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Usuario no identificado'
                    );
                }
            } else 
                {
                    $data = [
                    'status' => 'error',
                    'message' => 'Faltan datos',
                    'code' => 400
                            ];
                 }
        return response()->json($data, 200);
    }


    public function updateAntecedentes(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
        if ($checkToken) {
            $json = $request->input('json', null);
            $params = json_decode($json);
        
        $now =  Carbon::now()->timestamp;

        $antecedente = Antecedente::find($params->id);
        $antecedente->apers = $params->apers;
        $antecedente->afam = $params->afam;
        $antecedente->evol = $params->evol;
        $antecedente->modificado = $now;
            
        if($antecedente->save())
        {
            $res= [
                'status' => 'success',
                'message' => 'antecedente guardado con exito',
                'code' => 200
            ];
        }  else
        {
            $res= [
                'status' => 'error',
                'message' => 'error al guardar ant',
                'code' => 700
            ];
        }

        } else 
        {
            $res= [
                'status' => 'error',
                'message' => 'user no autorizad',
                'code' => 700
            ];
        }


        return response()->json($res,200);
    }

    public function store(Request $request)
        {     
                $hash = $request->header('Authorization');
                $jwtAuth = new JwtAuth();
                $checkToken = $jwtAuth->checkToken($hash);
                error_log('llego');
             
                if ($checkToken) 
                {
                    $json = $request->input('json', null);
                    $params = json_decode($json);
                    
                    $antecedente = Antecedente::find($params->idpac);
                    error_log(json_encode($params, JSON_PRETTY_PRINT));
                    if($antecedente == null)
                    {
                        $antecedente = new Antecedente();

                    }

                        $antecedente->idpac = $params->idpac;
                        $antecedente->creacion = $params->creacion;
                        $antecedente->apers = $params->apers;
                        $antecedente->afam = $params->afam;
                        $antecedente->evol = $params->evol;
                        $antecedente->modificado = $params->modificado;
                        error_log('lo guard');
                        $antecedente->save();

                       
                        $data = array
                        (
                            'antecedentes' => $antecedente,
                            'status' => 'success',
                            'code' => 200
                        );
                }   else 
                        {
                            $data = array
                            (
                                'message' => 'El usuario no esta autorizado',
                                'status' => 'error',
                                'code' => 400
                            );
                        }

                return response()->json($data, 200);
        }

}