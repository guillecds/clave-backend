<?php
namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Atencion;
use App\Pacientes; 
use Carbon\Carbon;
use DateTime;

class Listas extends Controller{

    public function getAtencionPorProfesional(Request $request, $id=null)
    {
        if($id)
        {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
        if ($checkToken) {
        date_default_timezone_set("America/Argentina/San_Juan");
            
            $atenciones=Atencion::selectRaw('pacientes.apynom as paciente, atencion.idcita as idcita, pacientes.*, citas.*, obrassociales.nombre as nameobrasocial, consultorios.domicilio as consultorio, profesionales.apynom as prof, atencion.id as idatencion')
            ->join('pacientes', 'idpac', '=', 'pacientes.id')
            ->join('citas', 'atencion.idcita', '=', 'citas.id')
            ->join('profesionales', 'profesionales.id', 'citas.idprof')
            ->join('obrassociales', 'pacientes.obrasocial', 'obrassociales.id')
            ->join('consultorios', 'consultorios.id', 'citas.idcons')
            ->where('atencion.idprof', $id)
            ->orderBy('atencion.fechalarga','ASC')
            ->get();


            foreach ($atenciones as $atencion)
            
            {
                $fecha= $atencion->fechalarga;
                $atencion->fechalarga = date("d-m-Y H:i",$fecha);
            }

   
            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' =>  $atenciones/*,
                'historia' => $historia*/

            );
        } else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
    } else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }

        return response()->json($data, 200);
            
    }

    

    
}
