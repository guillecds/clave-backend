<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImagesStore extends Controller
{
    public function storeImageBase64($path, $imageName) {
        if (isset($_FILES['imagenPropia'])) {

            $imagen_tipo = $_FILES['imagenPropia']['type'];
            $type = explode('/', $imagen_tipo)[1];
            $directorio_final = "storage/images/" . $path .'/' . $imageName . '.' . $type;
            $directorio_guardar = "images/" . $path . '/' . $imageName . '.' . $type;

            if ($imagen_tipo == "image/jpeg" || $imagen_tipo == "image/jpg" || $imagen_tipo == "image/png") {

                if (move_uploaded_file($_FILES['imagenPropia']['tmp_name'], $directorio_final)) {

                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'directorio' => $directorio_guardar, // SACAR EL STORAGE
                        'msj' => 'Imagen subida'
                    );
                    return response()->json($data, 200);
                } else {

                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'msj' => 'Error al mover imagen al servidor'
                    );
                    return response()->json($data, 200);
                }
            } else {

                $data = array(
                    'status' => 'error',
                    'code' => 500,
                    'msj' => 'Formato no soportado'
                );
                return response()->json($data, 200);
            }
        } else {

            $data = array(
                'status' => 'error',
                'code' => 400,
                'msj' => 'No se recibio ninguna imagen'
            );
            return response()->json($data, 200);
        }
}

    public function deleteImage (Request $request) {
    
        $json = $request->input('json',null);
        $params = json_decode($json);

    try {
       if (unlink('storage/' . $params)) {
            $res = array(
                'status' => 'success',
                'code' => 200,
                'msj' => 'Imagen borrada'
            );
       } else {
            $res = array(
                'status' => 'error',
                'code' => 400,
                'msj' => 'Error al borrar imagen del servidor'
            );
       }
    } catch (Exception  $e) {
        $res = array(
            'status' => 'error',
            'code' => 400,
            'msj' => $e->getMessage()
        );
    }
    
       return response()->json($res, 200);
    
    }
    
}
    