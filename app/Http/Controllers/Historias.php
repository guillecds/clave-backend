<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;
use App\Historia;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use DateTime;


class Historias extends Controller{



    public function getHistorias(Request $request, $id=null)
    {
        if($id)
        {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);
       
        if ($checkToken) {

      
        $historias=Historia::selectRaw('historia.id, historia.motivo, historia.fecha, historia.diagnostico, historia.tratamiento, historia.proximacita')
        ->where('historia.idpac', '=', $id)
        ->orderBy('fecha', 'DESC')
        ->get();

        foreach ($historias as $historia) {
            $fecha= $historia->fecha;
            $historia->fecha= date("d-m-Y H:i",$fecha);
        }

        $data = array(
            'status' => 'success',
            'code' => 200,
            'data' =>  $historias

        );


        }
        else {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }


        }
        
        else {
            $data = [
                'status' => 'error',
                'message' => 'Faltan datos',
                'code' => 400
            ];
        }

        return response()->json($data, 200);

    }

    public function storeHistoria(Request $request)
    {
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
         
            if ($checkToken) {

                $json = $request->input('json', null);
                $params = json_decode($json);
                $now =  Carbon::now()->timestamp;
                $historia = new Historia();
                $historia->idpac = $params->idpac;
                $historia->fecha = $now;
                $historia->motivo = $params->motivo;
                $historia->diagnostico = $params->diagnostico;
                $historia->tratamiento = $params->tratamiento;
                $historia->proximacita = $params->proximacita;
                $historia->save();
                    
                $data = array(
                    'historia' => $historia,
                    'status' => 'success',
                    'code' => 200
                );
       
            } else {
               
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }

            return response()->json($data, 200);
    }



}