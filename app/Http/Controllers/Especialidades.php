<?php
namespace App\Http\Controllers;
use App\Especialidad;
use Illuminate\Http\Request;
use App\Helpers\JwtAuth;

class Especialidades extends Controller
{

    public function getEspecialidades(Request $request)
    {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken)
        {
            $especialidades = Especialidad::select('especialidades.*')->orderBy('especialidad', 'ASC')
                ->get();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $especialidades
            );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }
        return response()->json($data, 200);
    }

    public function destroyEspecialidad(Request $request, $id = null)
    {
        // IMPROVE
        if ($id)
        {

            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
        
            if ($checkToken)
            {

                $especialidad = Especialidad::find($id);
                $especialidad->delete();

                if ($especialidad)
                {
                    $data = array(
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Especialidad borrada con exito',
                        'especialidad' => $especialidad
                    );
                }
                else
                {
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Ocurrio un problema al eliminar la especialidad.'
                    );
                }

            }
            else
            {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no autorizado'
                );
            }

        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado.'
            );
        }

        return response()->json($data, 200);
    }

    public function storeEspecialidad(Request $request)
    {
        $rules = ['especialidad' => 'required|unique:especialidades,especialidad|string'];

        if (!$this->validate($request, $rules)) return response()->json($error, 422);

        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken)
        {
            $params = $request->all();
            $especialidad = Especialidad::create($params);
            $data = array(
                'status' => 'success',
                'code' => 200,
                'message' => 'La especialidad fue cargada de forma exitosa.',
                'data' => $especialidad
            );
        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado.'
            );
        }
        return response()->json($data, 200);
    }

    public function updateEspecialidad(Request $request)
    {

        $rules = ['especialidad' => 'required|unique:especialidades,especialidad|string'];
        // if(!$this->validate($request, $rules))
        //improve
        //validar, error cuando quiere editar con el mismo dato q ya tiene
        // return response()->json($error, 422);
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if ($checkToken)
        {
            $especialidad = Especialidad::find($request->id);

            if ($request->has('especialidad'))
            {
                $especialidad->especialidad = $request->especialidad;
            }

            if (!$especialidad->save())
            {
                return $this->errorResponse('Fail updating especialidad', 409);
            }

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $especialidad,
                'message' => 'La especialidad fue actualizada de forma exitosa.'
            );

        }
        else
        {
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado.'
            );
        }

        return response()->json($data, 200);

    }

}

