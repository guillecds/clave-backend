<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Logg;
use Firebase\JWT\JWT;
use App\Helpers\JwtAuth;

class Loggs extends Controller
{
    public function getRegistros(Request $request) {
        $hash = $request->header('Authorization');
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);

        if($checkToken){
            $hoy = getdate();
            $from = $hoy['year'] . '-' . $hoy['mon'] . '-01 00:00:00';
            if ($hoy['mon'] == 12) {
                $anio = $hoy['year'] + 1;
                $mes = 1;
            } else {
                $anio = $hoy['year'];
                $mes = $hoy['mon'] + 1;
            }
            $to = $anio . '-' . $mes . '-01 00:00:00';

            $registros = Logg::whereBetween('created_at', [$from, $to])
            ->orderBy('created_at','desc')
            ->get();

            $data = array(
                'status' => 'success',
                'code' => 200,
                'data' => $registros
            );

        } else {

            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'Usuario no identificado'
            );
        }

        return response()->json($data, 200);
    }
}