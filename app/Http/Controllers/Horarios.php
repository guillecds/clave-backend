<?php
namespace App\Http\Controllers;

use App\Helpers\JwtAuth;
use Illuminate\Http\Request;
use App\Horario;


class Horarios extends Controller{

    public function getHorariosProfesional(Request $request, $id = null)
        {
           
            $hash = $request->header('Authorization');
            $jwtAuth = new JwtAuth();
            $checkToken = $jwtAuth->checkToken($hash);
         
            if ($checkToken) {
                
                $horarios = Horario::selectRaw('horarios.*, consultorios.domicilio as domicilio')
                ->where('horarios.idprof', '=', $id)
                ->join('consultorios', 'horarios.idcons', '=', 'consultorios.id')
                ->get();
            
                $data = array(
                    'status' => 'success',
                    'code' => 200,
                    'data' => $horarios
                );
            }  else  {

                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Usuario no identificado'
                );
                    }

            return response()->json($data, 200);

        }

        public function updateHorario(Request $request)
        {     
                $hash = $request->header('Authorization');
                $jwtAuth = new JwtAuth();
                $checkToken = $jwtAuth->checkToken($hash);
             
                if ($checkToken) {

                    $horario = Horario::find($request->id);
    
                    
                if ($request->has('idprof'))
                {
                    $horario->idprof = $request->idprof;
                }

                              
                if ($request->has('dia'))
                {
                    $horario->dia = $request->dia;
                }

                if ($request->has('horaini'))
                {
                    $horario->horaini = $request->horaini;
                }

                if ($request->has('minini'))
                {
                    $horario->minini = $request->minini;
                }

                if ($request->has('horalargai'))
                {
                    $horario->horalargai = $request->horalargai;
                }

                if ($request->has('horafin'))
                {
                    $horario->horafin = $request->horafin;
                }

                if ($request->has('minfin'))
                {
                    $horario->minfin = $request->minfin;
                }

                if ($request->has('horalargaf'))
                {
                    $horario->horalargaf = $request->horalargaf;
                }

                if ($request->has('idcons'))
                {
                    $horario->idcons = $request->idcons;
                }
                
                if (!$horario->save())
                {
                    return $this->errorResponse('Fail updating horario', 409);
                }
                    
                $data = array(
                    'horario' => $horario,
                    'status' => 'success',
                    'code' => 200
                );
       
            } else {
               
                $data = array(
                    'message' => 'El usuario no esta autorizado',
                    'status' => 'error',
                    'code' => 400
                );
            }

            return response()->json($data, 200);
        }


        public function storeHorario(Request $request)
        {
                $hash = $request->header('Authorization');
                $jwtAuth = new JwtAuth();
                $checkToken = $jwtAuth->checkToken($hash);
             
                if ($checkToken) {
    
                    $json = $request->input('json', null);
                    $params = json_decode($json);
                    
                    $horario = new Horario();
                    $horario->idprof = $params->idprof;
                    $horario->dia = $params->dia;
                    $horario->horaini = $params->horaini;
                    $horario->minini = $params->minini;
                    $horario->horalargai = $params->horalargai;
                    $horario->horafin = $params->horafin;
                    $horario->minfin = $params->minfin;
                    $horario->horalargaf = $params->horalargaf;
                    $horario->idcons = $params->idcons;
                    $horario->save();
                        
                    $data = array(
                        'horario' => $horario,
                        'status' => 'success',
                        'code' => 200
                    );
           
                } else {
                   
                    $data = array(
                        'message' => 'El usuario no esta autorizado',
                        'status' => 'error',
                        'code' => 400
                    );
                }
    
                return response()->json($data, 200);
        }

        public function destroyHorario(Request $request, $id=null)
        {
            if ($id) {
    
                $hash = $request->header('Authorization');
                $jwtAuth = new JwtAuth();
                $checkToken = $jwtAuth->checkToken($hash);
                $data = array();
    
                if($checkToken){
    
                    $horario = Horario::find($id);       
                    $horario->delete();
    
                    if ($horario) {
                        $data = array(
                            'status' => 'success',
                            'code' => 200,
                            'message' => 'Horario borrado con éxito',
                            'horario' => $horario
                        );
                    } else {
                        $data = array(
                            'status' => 'error',
                            'code' => 400,
                            'message' => ' ocurrio un problema al eliminar el horario'
                        );
                    }
    
                } else {
    
                    $data = array(
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Usuario no autorizado'
                    );
                }
    
            } else {
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Faltan datos'
                );
            }
    
            return response()->json($data, 200);
        }
    

}

