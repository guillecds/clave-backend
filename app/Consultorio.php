<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultorio extends Model
{   
    protected $fillable = [
        'idprof',
        'domicilio',
        'localidad',
        'provincia',
        'telefono',
        'nombre',
        'notas',
    ];
    protected $table = 'consultorios';
    protected $primaryKey = "id";
    
}
