<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ObraSocial extends Model
{   
    protected $fillable = [
    'nombre',
    'sigla',
    'descripcion'
    ];

    protected $table = 'obrassociales';
    protected $primaryKey = "id";
    
}
