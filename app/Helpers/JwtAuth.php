<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\User;

class JwtAuth
{
    public $key;

    public function __construct()
    {
        $this->key = 'laclavesecretaes123456789789456423';
    }

    public function signup($username, $password, $getToken = null)
    {
        //BUSCO QUE EL USUARIO EXISTA
        $user = User::where(
            array(
                'username' => $username,
                'password' => $password
            )
        )->first();

        if (is_object($user)) {  //SI EXISTE EL USUARIO OSEA SI ES UN OBJETO
            //GENERAR EL TOKEN Y DEVOLVERLO
            $token = array(
                'sub' => $user->id,
                'email' => $user->email,
                'username' => $user->username,
                'idprof' => $user->idprof,
                'rol' => $user->nivel,  // hay que cambiar a nivel 
                'iat' => time(),
                'exp' => time() + (60 * 60 * 24)
            );

            $jwt = JWT::encode($token, $this->key, 'HS256');


            if (is_null($getToken)) {
                return array(
                    'status' => 'ok',
                    'msj' => 'token devuelto',
                    'token' =>  $jwt,
                
                //    'menu' => $this->getMenu($user->rol) 
                );
            } else {
                $decode = JWT::decode($jwt, $this->key, array('HS256'));
                return array(
                    'status' => 'ok',
                    'msj' => 'usuario devuelto',
                    'usuario' =>  $decode
                );
            }
        } else {
            //DEVOLVER ERROR

            return array('status' => 'error', 'msj' => 'El usuario no existe');
        }
    }

    private function getEstablishment() {
        return getenv('establishment-durlero');
    }

    public function checkToken($jwt, $getIdentity = false)
    {
        $auth = false;

        try {
            $decode = JWT::decode($jwt, $this->key, array('HS256'));
        } catch (\UnexpectedValueException $exception) {
            $auth = false;
        } catch (\DomainException $exception) {
            $auth = false;
        }

        if (isset($decode) && is_object($decode) && isset($decode->sub)) {
            $auth = true;
        } else {
            $auth = false;
        }

        if ($getIdentity) {
            return $decode;
        } else {
            return $auth;
        }
    }

    public function getMenu($rol)
    {
        if ($rol == 'admin') {
        $menu = [
           [

                        'titulo' => 'Bandeja',
                        'icono' => 'mdi mdi-book-open-variant',
                     

                    ],
                [

                    'titulo' => 'Lista de Atencion',
                    'icono' => 'mdi mdi-book-open-variant',
                 

                ],
                [

                    'titulo' => 'Calendario',
                    'icono' => 'mdi mdi-book-open-variant',
                 

                ],
                [

                    'titulo' => 'Mensajes',
                    'icono' => 'mdi mdi-book-open-variant',
                 

                ],
                [

                    'titulo' => 'Administracion',
                    'icono' => 'mdi mdi-book-open-variant',
                 

                ],
                [

                    'titulo' => 'Configuración',
                    'icono' => 'mdi mdi-book-open-variant',
                    'submenu' => [

                        [

                            'titulo' => 'Especialidades'
                        ],
                        [

                            'titulo' => 'Obras Sociales'
                        ],
                        [

                            'titulo' => 'Usuarios'
                        ],
                        [

                            'titulo' => 'Relaciones'
                        ]

                    ]

                ]       
            ];
        }


        return $menu;
    }
}
