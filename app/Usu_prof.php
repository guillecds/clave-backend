<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usu_prof extends Model
{   
    protected $fillable = [
        'id_usuario',
        'id_profesional',
    ];

    protected $table = 'usu_prof';
    protected $primaryKey = "id";
    
}
