<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{   
    protected $fillable = [
        'especialidad'
    ];

    protected $table = 'especialidades';
    protected $primaryKey = "id";
    
}
